package com.mw.rpg.levels;

public class LevelLabel {

    private String levelId;

    private String levelName;

    public LevelLabel(String levelId, String levelName) {
        this.levelId = levelId;
        this.levelName = levelName;
    }

    public String getLevelId() {
        return levelId;
    }

    public String getLevelName() {
        return levelName;
    }
}
