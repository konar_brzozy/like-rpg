package com.mw.rpg.levels;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LevelConfigurationManagerImpl implements LevelConfigurationManager {

    private Map<String, LevelConfiguration> configurations;

    private List<LevelLabel> labelsList;

    LevelConfigurationManagerImpl() {
        labelsList = initLabels();
        configurations = initLevelConfigurations();
    }

    public LevelConfiguration levelConfiguration(String levelId) {
        return configurations.get(levelId);
    }

    public List<LevelLabel> labels() {
        return labelsList;
    }

    private List<LevelLabel> initLabels() {
        labelsList = new ArrayList<>();
        // labels.add(new LevelLabel("level02", "level02"));
        labelsList.add(new LevelLabel("jungle001", "001 Jungle"));
        return labelsList;
    }

    private Map<String, LevelConfiguration> initLevelConfigurations(){
        Map<String, LevelConfiguration> configurations = new HashMap<>();

        // configurations.put(labels.get(0).getLevelId(), level02(labels.get(0)));
        configurations.put(labelsList.get(0).getLevelId(), levelJungle001(labelsList.get(0)));
        return configurations;
    }

    private LevelConfiguration level02(LevelLabel label){
        LevelConfiguration levelConfiguration = new LevelConfiguration();
        levelConfiguration.setLevelId(label.getLevelId());
        levelConfiguration.setName(label.getLevelName());
        levelConfiguration.setMap("maps/level02.tmx");
        levelConfiguration.setPlayerStartX(20);
        levelConfiguration.setPlayerStartY(9);

        Map<Vector2, String> exits = new HashMap<>();
        exits.put(new Vector2(25f, 0f ), "cave");
        levelConfiguration.setExits(exits);

//        StationarySprite burger = new StationarySprite("images/item-burger.png", 17, 9, 6, 48, 0.3f);
//        StationarySprite burger2 = new StationarySprite("images/item-burger.png", 17, 10, 6, 48, 0.3f);
//        burger.setName("burger");
//        burger2.setName("burger");
//
//        stationarySprites.add(burger);
//        stationarySprites.add(burger2);

        return levelConfiguration;
    }

    private LevelConfiguration levelJungle001(LevelLabel label){
        LevelConfiguration levelConfiguration = new LevelConfiguration();
        levelConfiguration.setLevelId(label.getLevelId());
        levelConfiguration.setName(label.getLevelName());
        levelConfiguration.setMap("maps/001_jungle.tmx");
        levelConfiguration.setPlayerStartX(0);
        levelConfiguration.setPlayerStartY(0);

        Map<Vector2, String> exits = new HashMap<>();
        exits.put(new Vector2(0f, 17f), "finish");
        exits.put(new Vector2(0f, 18f), "finish");
        exits.put(new Vector2(0f, 19f), "finish");
        exits.put(new Vector2(0f, 20f), "finish");
        levelConfiguration.setExits(exits);

        return levelConfiguration;
    }
}
