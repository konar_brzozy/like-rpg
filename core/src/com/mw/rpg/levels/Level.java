package com.mw.rpg.levels;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.mw.rpg.sprites.Player;
import com.mw.rpg.sprites.Sprite;
import com.mw.rpg.sprites.StationarySprite;

import java.util.HashMap;
import java.util.Map;


public class Level {

    protected TiledMap map;
    protected Array<Sprite> sprites;
    protected Array<StationarySprite> stationarySprites;
    protected Map<Vector2, String> exits;
    protected float red;
    protected float green;
    protected float blue;

    private LevelConfiguration configuration;

    Level() {
        sprites = new Array<>();
        stationarySprites = new Array<>();
        exits = new HashMap<>();
    }

    public Level(LevelConfiguration configuration) {
        this();

        this.configuration = configuration;
        map = new TmxMapLoader().load(configuration.getMap());
        sprites.add(new Player(configuration.getPlayerStartX(), configuration.getPlayerStartY()));
        exits.putAll(configuration.getExits());

        red = 74f/255f;
        green = 160f/255f;
        blue = 223f/255f;
    }

    public void setStationarySprites(Array<StationarySprite> stationarySprites) {
        this.stationarySprites = stationarySprites;
    }

    public Array<StationarySprite> getStationarySprites() {
        return stationarySprites;
    }

    public TiledMap getMap() {
        return map;
    }

    public Array<Sprite> getSprites() {
        return sprites;
    }

    public float getRed() {
        return red;
    }

    public float getGreen() {
        return green;
    }

    public float getBlue() {
        return blue;
    }

    public Map<Vector2, String> getExits() {
        return exits;
    }
}
