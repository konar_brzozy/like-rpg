package com.mw.rpg.levels;


import java.util.List;

public class LevelManager {

    private List<LevelLabel> levelLabels;

    private LevelConfigurationManager configurationManager = new LevelConfigurationManagerImpl();


    public List<LevelLabel> getLevelLabels() {
        if (levelLabels == null) {
            levelLabels = configurationManager.labels();
        }
        return levelLabels;
    }

    public Level level(String levelId) {
        LevelConfiguration configuration = configurationManager.levelConfiguration(levelId);
        return new Level(configuration);
    }
}
