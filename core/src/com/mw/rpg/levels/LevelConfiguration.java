package com.mw.rpg.levels;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.Map;

class LevelConfiguration {

    private String levelId;

    private String name;

    private String map;

    private float playerStartX;

    private float playerStartY;

    private Map<Vector2, String> exits;

    public String getMap() {
        return map;
    }

    float getPlayerStartX() {
        return playerStartX;
    }

    float getPlayerStartY() {
        return playerStartY;
    }

    Map<Vector2, String> getExits() {
        return exits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMap(String map) {
        this.map = map;
    }

    void setPlayerStartX(float playerStartX) {
        this.playerStartX = playerStartX;
    }

    void setPlayerStartY(float playerStartY) {
        this.playerStartY = playerStartY;
    }

    void setExits(Map<Vector2, String> exits) {
        this.exits = exits;
    }

    public String getLevelId() {
        return levelId;
    }

    void setLevelId(String levelId) {
        this.levelId = levelId;
    }
}