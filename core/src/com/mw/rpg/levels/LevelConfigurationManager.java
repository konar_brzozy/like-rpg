package com.mw.rpg.levels;

import java.util.List;

interface LevelConfigurationManager {

    LevelConfiguration levelConfiguration(String levelId);

    List<LevelLabel> labels();
}