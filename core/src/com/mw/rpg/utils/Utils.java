package com.mw.rpg.utils;

import com.badlogic.gdx.Gdx;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

    public static float roundToTile(float value){
        return new BigDecimal(value).setScale(0, RoundingMode.HALF_EVEN).floatValue();
    }

    public static float cutDecimals(float value){
        return new BigDecimal(value).setScale(0, RoundingMode.FLOOR).floatValue();
    }

    public static float cutDecimalsUp(float value){
        return new BigDecimal(value).setScale(0, RoundingMode.CEILING).floatValue();
    }

    public static void log(String message){
        Gdx.app.log("like-rpg", message);
    }
}
