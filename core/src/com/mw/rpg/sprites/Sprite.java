package com.mw.rpg.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mw.rpg.Creature;



public class Sprite extends Creature implements Comparable<Sprite> {
    
    public static float SIZE = 1f;

    public enum State {
        standFront,
        standBack,
        standLeft,
        standRight,

        walkFront,
        walkBack,
        walkLeft,
        walkRight,

        attackFront,
        attackBack,
        attackLeft,
        attackRight,

        death
    }

    private State state;
    private boolean facingLeft;
    Texture texture;
    private Vector2 position;

    private Vector2 touchPosition;
    private float animationTime;

    private TextureRegion currentFrame;
    private TextureRegion currentFrameTop;

    Animation standFrontAnimation;
    Animation standBackAnimation;
    Animation standLeftAnimation;
    Animation standRightAnimation;
    Animation walkFrontAnimation;
    Animation walkBackAnimation;
    Animation walkLeftAnimation;
    Animation walkRightAnimation;
    Animation attackFrontAnimation;
    Animation attackBackAnimation;
    Animation attackLeftAnimation;
    Animation attackRightAnimation;
    Animation deathAnimation;

    private boolean shouldGoX(){
       return madeMoveX() && (moveRight() || moveLeft());
    }

    private boolean shouldGoY(){
       return madeMoveY() && (moveDown() || moveUp());
    }

    public boolean shouldGo(){
        return shouldGoX() || shouldGoY();
    }

    public boolean madeMoveX(){
        return getTouchX() != 0f;
    }

    public boolean madeMoveY(){
        return getTouchY() != 0f;
    }

    public boolean moveRight(){
        return getTouchX() > getX();
    }

    public boolean moveLeft(){
        return getTouchX() < getX();
    }

    public boolean moveUp(){
        return getTouchY() > getY();
    }

    public boolean moveDown(){
        return getTouchY() < getY();
    }

    public Sprite(String textureFile, float x, float y) {
        texture = new Texture(Gdx.files.internal(textureFile));
        position = new Vector2();
        touchPosition = new Vector2();
        animationTime = 0f;
        setPosition(x, y);
        state = State.standFront;
        facingLeft = false;
    }

    TextureRegion[] getFrames(Texture texture, int row, int columns, int width, int height) {
        TextureRegion[][] tmp = TextureRegion.split(texture, width, height);
        TextureRegion[] frames = new TextureRegion[columns];
        int index = 0;
        for (int i = 0; i < columns; i++) {
            frames[index++] = tmp[row][i];
        }
        return frames;
    }

    public TextureRegion getCurrentFrame() {
        return currentFrame;
    }

    public void setCurrentFrame(TextureRegion currentFrame) {
        this.currentFrame = currentFrame;
    }

    public TextureRegion getCurrentFrameTop() {
        return currentFrameTop;
    }

    public void setCurrentFrameTop(TextureRegion currentFrame) {
        this.currentFrameTop = new TextureRegion(currentFrame,
                                                 currentFrame.getRegionX(), currentFrame.getRegionY() + 48,
                                                 currentFrame.getRegionWidth(), currentFrame.getRegionHeight());
    }

    public float getX() {
        return position.x;
    }

    private float getTouchX() {
        return touchPosition.x;
    }

    public void setX(float x) {
        this.position.x = x;
    }


    public float getY() {
        return position.y;
    }

    private float getTouchY() {
        return touchPosition.y;
    }

    public void setY(float y) {
        this.position.y = y;
    }


    public void setTouchPosition(float x, float y) {
        this.touchPosition.set(x, y);
    }

    private void setPosition(float x, float y) {
        this.position.set(x, y);
    }

    public Texture getTexture() {
        return texture;
    }

    float getAnimationTime() {
        return animationTime;
    }

    public void setAnimationTime(float animationTime) {
        this.animationTime += animationTime;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isFacingLeft() {
        return facingLeft;
    }

    public TextureRegion getStandFrontFrame() {
        return (TextureRegion) standFrontAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getStandBackFrame() {
        return (TextureRegion) standBackAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getStandLeftFrame() {
        return (TextureRegion) standLeftAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getStandRightFrame() {
        return (TextureRegion) standRightAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getWalkFrontFrame() {
        return (TextureRegion) walkFrontAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getWalkBackFrame() {
        return (TextureRegion) walkBackAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getWalkLeftFrame() {
        return (TextureRegion) walkLeftAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getWalkRightFrame() {
        return (TextureRegion) walkRightAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getAttackFrontFrame() {
        return (TextureRegion) attackFrontAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getAttackBackFrame() {
        return (TextureRegion) attackBackAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getAttackLeftFrame() {
        return (TextureRegion) attackLeftAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getAttackRightFrame() {
        return (TextureRegion) attackRightAnimation.getKeyFrame(animationTime, true);
    }

    public TextureRegion getDeathFrame() {
        return (TextureRegion) deathAnimation.getKeyFrame(animationTime, true);
    }

    private String comparableY() {
        return Float.toString(getY());
    }

    @Override
    public int compareTo(Sprite other) {
        return other.comparableY().compareTo(comparableY());

    }
}
