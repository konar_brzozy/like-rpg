package com.mw.rpg.sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;
import com.mw.rpg.classes.Wizard;
import com.mw.rpg.races.Human;


public class Player extends Sprite {

    private static final String PLAYER_TEXTURE = "images/$041-Mage09.png";
    private static final int WIDTH_PLAYER = 32;
    private static final int HEIGHT_PLAYER = 48;

    public Player(float x, float y) {
        super(PLAYER_TEXTURE, x, y);

        loadTextures(WIDTH_PLAYER, HEIGHT_PLAYER);

        // General
        name = "Player";
        race = new Human();
        classes = new Array<>();
        classes.add(new Wizard());
        HP = 10;
        speed = 6;
        initiative = 1;
        moveActions = 2;
        attackActions = 1;
    }

    private void loadTextureWithoutAnimations(int width, int height){
        standFrontAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        standBackAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        standLeftAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        standRightAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        walkFrontAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        walkBackAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        walkLeftAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
        walkRightAnimation = new Animation<>(0.3f, getFrames(texture, 0, 1, width, height));
    }

    private void loadTextures(int width, int height) {
        standFrontAnimation = new Animation<>(0.3f, getFrames(texture, 0, 3, width, height));
        standBackAnimation = new Animation<>(0.3f, getFrames(texture, 3, 3, width, height));
        standLeftAnimation = new Animation<>(0.3f, getFrames(texture, 1, 3, width, height));
        standRightAnimation = new Animation<>(0.3f, getFrames(texture, 2, 3, width, height));
        walkFrontAnimation = new Animation<>(0.3f, getFrames(texture, 0, 3, width, height));
        walkBackAnimation = new Animation<>(0.3f, getFrames(texture, 3, 3, width, height));
        walkLeftAnimation = new Animation<>(0.3f, getFrames(texture, 1, 3, width, height));
        walkRightAnimation = new Animation<>(0.3f, getFrames(texture, 2, 3, width, height));
    }

    private void loadTexturesOld(int width, int height){
        // Animations
        standFrontAnimation = new Animation<>(0.3f, getFrames(texture, 9, 6, width, height));
        standBackAnimation = new Animation<>(0.3f, getFrames(texture, 8, 6, width, height));
        standLeftAnimation = new Animation<>(0.3f, getFrames(texture, 7, 6, width, height));
        standRightAnimation = new Animation<>(0.3f, getFrames(texture, 7, 6, width, height));
        walkFrontAnimation = new Animation<>(0.3f, getFrames(texture, 6, 4, width, height));
        walkBackAnimation = new Animation<>(0.3f, getFrames(texture, 5, 4, width, height));
        walkLeftAnimation = new Animation<>(0.3f, getFrames(texture, 4, 4, width, height));
        walkRightAnimation = new Animation<>(0.3f, getFrames(texture, 4, 4, width, height));
        attackFrontAnimation = new Animation<>(0.3f, getFrames(texture, 3, 4, width, height));
        attackBackAnimation = new Animation<>(0.3f, getFrames(texture, 2, 4, width, height));
        attackLeftAnimation = new Animation<>(0.3f, getFrames(texture, 1, 4, width, height));
        attackRightAnimation = new Animation<>(0.3f, getFrames(texture, 1, 4, width, height));
        deathAnimation = new Animation<>(0.3f, getFrames(texture, 0, 4, width, height));
    }
}