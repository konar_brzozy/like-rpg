package com.mw.rpg.sprites;

public enum SpriteType {
    ITEM, PERSON, BURGER
}
