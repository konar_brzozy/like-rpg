package com.mw.rpg;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.mw.rpg.levels.Level;
import com.mw.rpg.sprites.Sprite;
import com.mw.rpg.sprites.SpriteUpComparator;
import com.mw.rpg.sprites.StationarySprite;
import com.mw.rpg.utils.Utils;

public class View {
    private Array<Sprite> sprites;
    private Array<Sprite> sortedUpSprites;
    private Array<StationarySprite> stationarySprites;
    private float red;
    private float green;
    private float blue;

    private Stage stage;

    private OrthographicCamera camera;

    private OrthogonalTiledMapRenderer mapRenderer;
    private int[] baseLayer;
    private int[] underLayer1;
    private int[] underLayer2;
    private int[] overLayer;

    private SpriteBatch spriteBatch;

    private Label labelValue;

    private Array<BitmapFont> fonts;

    public View(Level level, Game game) {
        stage = new Stage();

        GameInputProcessor inputProcessor = new GameInputProcessor(game);
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(inputProcessor);
        Gdx.input.setInputProcessor(multiplexer);

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth() / 100f, Gdx.graphics.getHeight() / 100f);
        camera.update();

        sprites = level.getSprites();
        stationarySprites = level.getStationarySprites();
        red = level.getRed();
        green = level.getGreen();
        blue = level.getBlue();
        spriteBatch = new SpriteBatch();

        TiledMap map = level.getMap();
        mapRenderer = new OrthogonalTiledMapRenderer(map, 1 / 48f);
        baseLayer = new int[1];
        baseLayer[0] = 0;
        underLayer1 = new int[1];
        underLayer1[0] = 1;
        underLayer2 = new int[1];
        underLayer2[0] = 3;
        overLayer = new int[1];
        overLayer[0] = 2;


        setupHUD();
    }



    public boolean removeStationarySprites(StationarySprite stationarySprite){
        return stationarySprites.removeValue(stationarySprite, true);
    }

    public void render() {
        labelValue.setText(Integer.toString(sprites.get(0).itemsSize()));

        Gdx.gl.glClearColor(red, green, blue, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.position.set(sprites.get(0).getX() + 1, sprites.get(0).getY(), 0);
        camera.update();

        spriteBatch.setProjectionMatrix(camera.combined);

        mapRenderer.setView(camera);

        mapRenderer.render(baseLayer);
        mapRenderer.render(underLayer1);
        mapRenderer.render(underLayer2);

        sortedUpSprites = new Array<>(sprites);
        sortedUpSprites.sort(new SpriteUpComparator());

        spriteBatch.begin();
        renderStationarySprites();
        renderSprites();
        renderStationarySpritesTop();
        spriteBatch.end();

        mapRenderer.render(overLayer);
        labelValue.setText(Integer.toString(sprites.get(0).itemsSize()));

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    public void resize(int width, int height) {
        stage.setViewport(new ExtendViewport(width, height));
    }

    private void renderSprites() {
        for (Sprite sprite : sortedUpSprites) {
            sprite.setAnimationTime(Gdx.graphics.getDeltaTime());

            switch (sprite.getState()) {
                case standFront:
                    sprite.setCurrentFrame(sprite.getStandFrontFrame());
                    break;
                case standBack:
                    sprite.setCurrentFrame(sprite.getStandBackFrame());
                    break;
                case standLeft:
                    sprite.setCurrentFrame(sprite.getStandLeftFrame());
                    break;
                case standRight:
                    sprite.setCurrentFrame(sprite.getStandRightFrame());
                    break;
                case walkFront:
                    sprite.setCurrentFrame(sprite.getWalkFrontFrame());
                    break;
                case walkBack:
                    sprite.setCurrentFrame(sprite.getWalkBackFrame());
                    break;
                case walkLeft:
                    sprite.setCurrentFrame(sprite.getWalkLeftFrame());
                    break;
                case walkRight:
                    sprite.setCurrentFrame(sprite.getWalkRightFrame());
                    break;
                case attackFront:
                    sprite.setCurrentFrame(sprite.getAttackFrontFrame());
                    break;
                case attackBack:
                    sprite.setCurrentFrame(sprite.getAttackBackFrame());
                    break;
                case attackLeft:
                    sprite.setCurrentFrame(sprite.getAttackLeftFrame());
                    break;
                case attackRight:
                    sprite.setCurrentFrame(sprite.getAttackRightFrame());
                    break;
                case death:
                    sprite.setCurrentFrame(sprite.getDeathFrame());
                    break;
            }

            if (!sprite.isFacingLeft()) {
                spriteBatch.draw(sprite.getCurrentFrame(), sprite.getX(), sprite.getY(),
                                 Sprite.SIZE, Sprite.SIZE);
            } else {
                spriteBatch.draw(sprite.getCurrentFrame(), sprite.getX() + Sprite.SIZE, sprite.getY(),
                                 -Sprite.SIZE, Sprite.SIZE);
            }
        }
    }

    private void renderStationarySprites() {
        for (StationarySprite stationarySprite : stationarySprites) {
            stationarySprite.setAnimationTime(Gdx.graphics.getDeltaTime());

            stationarySprite.setCurrentFrame(stationarySprite.getAnimationFrame());

            spriteBatch.draw(stationarySprite.getCurrentFrame(), stationarySprite.getX(), stationarySprite.getY(),
                             stationarySprite.getSize(), stationarySprite.getSize());
        }
    }

    private void renderStationarySpritesTop() {
        for (StationarySprite stationarySprite : stationarySprites) {
            spriteBatch.draw(stationarySprite.getHeadTexture(),
                             stationarySprite.getX(), stationarySprite.getY() + 1,
                             stationarySprite.getSize(), stationarySprite.getSize());
        }
    }

    public void getInput() {
        if (Gdx.input.justTouched()) {
            Vector3 point = getWorldPoint(Gdx.input.getX(), Gdx.input.getY());
            moveTo(point);
        }
    }

    private void moveTo(Vector3 vector){
        Utils.log("moveTo: "+ Utils.roundToTile(vector.x) + ", "+Utils.roundToTile(vector.y));
        sprites.get(0).setTouchPosition(Utils.roundToTile(vector.x), Utils.roundToTile(vector.y));
    }

    private Vector3 getWorldPoint(float x, float y) {
        Utils.log("point: "+ x + ", "+y);
        Vector3 worldPoint = new Vector3(x, y, 0);
        camera.unproject(worldPoint);
        return worldPoint;
    }

    public Stage getStage() {
        return stage;
    }

    public Array<BitmapFont> getFonts() {
        return fonts;
    }


    private void setupHUD() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/advocut-webfont.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 30;
        parameter.borderWidth = 1;
        parameter.color = Color.WHITE;
        parameter.shadowOffsetX = 3;
        parameter.shadowOffsetY = 3;
        parameter.shadowColor = new Color(0, 0.5f, 0, 0.75f);
        BitmapFont font25 = generator.generateFont(parameter); // font size 24 pixels
        generator.dispose();

        Skin skin = new Skin();

        // Generate a 1x1 white texture and store it in the skin named "white".
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white", new Texture(pixmap));

        // Store the default libgdx font under the name "default".
        skin.add("default", font25);

        // Configure a TextButtonStyle and name it "default".
        // Skin resources are stored by type, so this doesn't overwrite the font.
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.background = skin.newDrawable("white", Color.DARK_GRAY);
        labelStyle.font = skin.getFont("default");
        skin.add("default", labelStyle);

        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();
        table.setFillParent(true);
        table.bottom().left();
        stage.addActor(table);

        Label label = new Label("    burgers:  ", skin);
        labelValue = new Label(Integer.toString(sprites.get(0).itemsSize()), skin);
        table.add(label);
        table.add(labelValue).width(50);

    }
}