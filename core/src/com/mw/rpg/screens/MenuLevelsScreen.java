package com.mw.rpg.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mw.rpg.levels.LevelLabel;
import com.mw.rpg.levels.LevelManager;
import com.mw.rpg.utils.Utils;

import java.util.List;

public class MenuLevelsScreen implements Screen {

    private Stage stage;

    private Skin skinButton;
    private Game game;

    MenuLevelsScreen(final Game game) {
        this.game = game;
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        Gdx.input.setCatchBackKey(false);

        skinButton = new Skin(Gdx.files.internal("skin/craftacular-ui.json"));

        LevelManager  levelManager = new LevelManager();
        List<LevelLabel> labels = levelManager.getLevelLabels();

        Container<Table> tableContainer = new Container<>();

        float sw = Gdx.graphics.getWidth();
        float sh = Gdx.graphics.getHeight();

        float cw = sw * 0.7f;
        float ch = sh * 0.5f;

        tableContainer.setSize(cw, ch);
        tableContainer.setPosition((sw - cw) / 2.0f, (sh - ch) / 2.0f);
        tableContainer.fillX();

        Table table = new Table(skinButton);

        table.row().expandX().fillX();;

        table.row().fillX().expandX();
        table.add(gameButton(labels.get(0))).height(ch/3.0f).width(cw/2.0f).padBottom(10f);
//        table.row().fillX().expandX();
//        table.add(continueGameButton()).height(ch/3.0f).width(cw/2.0f).padBottom(10f);
//
//        table.row().fillX().expandX();
//        table.add(aboutButton()).height(ch/3.0f).width(cw/2.0f).padBottom(10f);
        tableContainer.setActor(table);
        stage.addActor(tableContainer);
    }

    private TextButton gameButton(final LevelLabel levelLabel){
        TextButton button = new TextButton(levelLabel.getLevelName(), skinButton);
        button.getLabel().setFontScale(2.5f, 2.5f);
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game, levelLabel.getLevelId()));
            }
        });
        return button;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.act(delta);

        Gdx.gl.glClearColor(1, 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }

    @Override
    public void resize(int width, int height) { }

    @Override
    public void pause() { }

    @Override
    public void resume() { }

    @Override
    public void hide() { }

    @Override
    public void dispose() { }
}
