package com.mw.rpg.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.mw.rpg.View;
import com.mw.rpg.levels.Level;
import com.mw.rpg.levels.LevelManager;
import com.mw.rpg.sprites.Sprite;
import com.mw.rpg.sprites.StationarySprite;
import com.mw.rpg.utils.Utils;

import java.util.List;

public class GameScreen implements Screen {

    LevelManager levelManager;

    private static final float SPEED = 8f;

    private Game game;

    private static final float STEP = 1f;

    private TiledMap map;

    private Array<Sprite> sprites;

    private Level currentLevel;

    private View view;

    private boolean stateChanged;

    public GameScreen(Game game) {
        this.game = game;

        Gdx.input.setCatchBackKey(true);
    }

    public GameScreen(Game game, String levelId) {
        this(game);

        levelManager = new LevelManager();
        currentLevel = levelManager.level(levelId);
    }

    private Pool<View> viewPool = new Pool<View>() {

        @Override
        protected View newObject() {
            return new View(currentLevel, game);
        }
    };




//    @Override
//    public void create() {
//
//    }


    @Override
    public void resize(int width, int height) {
        view.resize(width, height);
    }


    @Override
    public void pause() {
    }


    @Override
    public void resume() {
    }

    @Override
    public void hide() {

    }


    @Override
    public void show() {
        map = currentLevel.getMap();
        sprites = currentLevel.getSprites();
        view = viewPool.obtain();
        stateChanged = true;
    }

    @Override
    public void render(float delta) {
        if (stateChanged) {
            setupLevel();
            stateChanged = false;
        }

        view.getInput();
        updateSprites(Gdx.graphics.getDeltaTime());
        view.render();
    }


    @Override
    public void dispose() {

            for (StationarySprite sprite : currentLevel.getStationarySprites()) {
                sprite.getTexture().dispose();
            }
            for (Sprite sprite : currentLevel.getSprites()) {
                sprite.getTexture().dispose();
            }
            currentLevel.getMap().dispose();

        view.getStage().dispose();
        for (BitmapFont font : view.getFonts()) {
            font.dispose();
        }
    }


    private void updateSprites(float deltaTime) {
        for (Sprite sprite : sprites) {
            if(sprite.shouldGo()){
                float step = deltaTime * SPEED;
                moveX(sprite, step);
                moveY(sprite, step);
            } else {
                sprite.setState(Sprite.State.standFront);
            }
        }
    }


    private void moveX(Sprite sprite, float step){

        if (sprite.madeMoveX()) {
            if (sprite.moveRight()) {
                sprite.setState(Sprite.State.walkRight);

                if(isEmpty(Utils.cutDecimals(sprite.getX()) + STEP, sprite.getY()) || sprite.getX() % 1 !=0){
                    float destination = sprite.getX() + step;
                    if(destination > Utils.cutDecimals(sprite.getX()) + STEP){
                        destination = Utils.cutDecimals(destination);
                    }
                    sprite.setX(destination);

                    exit(sprite.getX(), sprite.getY());
                    item(sprite.getX(), sprite.getY());
                } else {
                    sprite.setState(Sprite.State.standFront);
                }

            }


            if (sprite.moveLeft()) {
                    sprite.setState(Sprite.State.standLeft);
                if (isEmpty(Utils.cutDecimalsUp(sprite.getX()) - STEP, sprite.getY()) || sprite.getX() % 1 !=0) {

                    float destination = sprite.getX() - step;
                    if(destination < Utils.cutDecimalsUp(sprite.getX()) - STEP){
                        destination = Utils.cutDecimalsUp(destination);
                    }
                    sprite.setX(destination);

                    exit(sprite.getX(), sprite.getY());
                    item(sprite.getX(), sprite.getY());
                } else {
                    sprite.setState(Sprite.State.standFront);
                }
            }
        }
    }

    private void moveY(Sprite sprite, float step){
//        Utils.log("touch:  "+sprite.getTouchY()+", x: "+sprite.getY());

        if (sprite.madeMoveY()) {

            if (sprite.moveUp()) {
                    sprite.setState(Sprite.State.walkBack);
                if(isEmpty(sprite.getX(), Utils.cutDecimals(sprite.getY()) + STEP) || sprite.getY() % 1 !=0) {
                    float destination = sprite.getY() + step;
                    if(destination > Utils.cutDecimals(sprite.getY()) + STEP){
                        destination = Utils.cutDecimals(destination);
                    }
                    sprite.setY(destination);

                    exit(sprite.getX(), sprite.getY());
                    item(sprite.getX(), sprite.getY());
                } else {
                    sprite.setState(Sprite.State.standFront);
                }
            }

            if (sprite.moveDown()) {
                    sprite.setState(Sprite.State.walkFront);
                if(isEmpty(sprite.getX(), Utils.cutDecimalsUp(sprite.getY()) - STEP) || sprite.getY() % 1 !=0) {
                    float destination = sprite.getY() - step;
                    if(destination < Utils.cutDecimalsUp(sprite.getY()) - STEP){
                        destination = Utils.cutDecimalsUp(destination);
                    }
                    sprite.setY(destination);

                    exit(sprite.getX(), sprite.getY());
                    item(sprite.getX(), sprite.getY());
                }

            }
        }
    }

    private boolean isEmpty(float x, float y) {
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(3);
        TiledMapTileLayer.Cell cell = layer.getCell(Math.round(x), Math.round(y));
        if(cell == null){
            return true;
        }
        return false;
    }

    private void exit(float x, float y) {
        for (Vector2 point : currentLevel.getExits().keySet()) {
            if (point.x == x && point.y == y) {
//                state = states.get(currentLevel.getExits().get(point));
//                stateChanged = true;
            }
        }
    }

    private void item(float x, float y) {
        for (StationarySprite stationarySprite : currentLevel.getStationarySprites()) {
            if (stationarySprite.getX() == x && stationarySprite.getY() == y) {
                if(view.removeStationarySprites(stationarySprite)){
                    sprites.get(0).addItem(stationarySprite.getName());
//                    stateChanged = true;
                }
            }
        }
    }

    private void setupLevel() {
        map = currentLevel.getMap();
        sprites = currentLevel.getSprites();
        viewPool.free(view);
        viewPool.clear();
        view = viewPool.obtain();
    }
}