package com.mw.rpg;

import com.badlogic.gdx.Game;
import com.mw.rpg.screens.SplashScreen;

public class LikeRPG extends Game {

    @Override
    public void create () {
        this.setScreen(new SplashScreen(this));
    }

    @Override
    public void render() {
        super.render();
    }
}
