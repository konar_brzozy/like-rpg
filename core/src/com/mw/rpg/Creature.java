package com.mw.rpg;

import com.badlogic.gdx.utils.Array;

import com.mw.rpg.classes.GameClass;
import com.mw.rpg.races.Race;

import java.util.ArrayList;
import java.util.List;

public class Creature {

    protected String name;
    protected Race race;
    protected Array<GameClass> classes;
    protected int HP;
    protected int speed;
    protected int initiative;
    protected int moveActions;
    protected int attackActions;
    private List<String> items = new ArrayList<>();

    public void addItem(String itemName){
        items.add(itemName);
    }

    public int itemsSize(){
        return items.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
